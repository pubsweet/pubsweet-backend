# DEPRECATED: this project has been renamed to pubsweet-server

The new repo can be found at https://gitlab.coko.foundation/pubsweet/pubsweet-server.

This repository has been deprecated, but will be preserved here for compatibility of older installs.